# GamesAndKeys.com

GamesAndKeys is a website that provides users with free video game piano tutorials, sheet music, and midi files.<br />
<i>* New site still in development. GamesAndKeys.com currently goes to the old blogger version.</i>

## Features

* Google Login
* Watch videos of video game piano tutorials
* Download sheet music (pdf)
* Download midi files of piano tutorials

## Usage

### Logging in to `Games and Keys`
#### (These are future steps. GamesAndKeys.com currently goes to old site)
To login, visit [GamesAndKeys.com](http://www.gamesandkeys.com) and click the login button. You will be redirected to the Google login. After successfully logging in, you will be redirected to the `Games and Keys` dashboard.

### Submitting a Request

To submit a request for a video game piano tutorial, click on Requests tab. Then, use the form to submit a request.

### Accessing Content

To view videos or to download files, click on the title of the game. Then, select your desired song. You will be redirected to the page that has the video and files.

# Development

### Front-End technologies

* React
* Redux
* Material-UI

### Backend technologies

* Node
* Express

### Database

* SQL

### Testing

* Jest
* Enzyme

# Sending Feedback

I am always open to [your feedback](https://github.com/MightyJoeW/gamesandkeys-webapp/issues).
